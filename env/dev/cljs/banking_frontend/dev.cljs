(ns ^:figwheel-no-load banking-frontend.dev
  (:require
    [banking-frontend.core :as core]
    [devtools.core :as devtools]))

(devtools/install!)

(enable-console-print!)

(core/init!)
