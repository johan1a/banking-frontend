FROM anapsix/alpine-java:latest

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY target/banking-frontend.jar /usr/src/app/

CMD ["java", "-jar", "banking-frontend.jar"]
