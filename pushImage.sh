#!/bin/sh
TAG=$(git rev-parse HEAD)
docker push johan1a/banking-frontend:${TAG}
docker push johan1a/banking-frontend:latest

docker push johan1a/banking-frontend:dev-${TAG}
docker push johan1a/banking-frontend:dev-latest
