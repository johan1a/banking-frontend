def label = "worker-${UUID.randomUUID().toString()}"

podTemplate(label: label,
  serviceAccount: 'jenkins-master',
  containers: [
      containerTemplate(name: 'docker', image: 'docker', command: 'cat', ttyEnabled: true),
      containerTemplate(name: 'clojure', image: 'clojure', ttyEnabled: true),
      containerTemplate(name: 'kubectl', image: 'lachlanevenson/k8s-kubectl:v1.8.8', command: 'cat', ttyEnabled: true),
],
volumes: [
  hostPathVolume(mountPath: '/var/run/docker.sock', hostPath: '/var/run/docker.sock')
]) {
  node(label) {
    def myRepo = checkout scm
    def gitCommit = myRepo.GIT_COMMIT

    stage('Build dev application') {
      container('clojure') {
          sh """
            lein clean
            lein with-profile +uberjar,+dockerdev uberjar
            """
      }
    }

    stage('Build and push dev Docker image') {
      container('docker') {
      withCredentials([[$class: 'UsernamePasswordMultiBinding',
                           credentialsId: 'docker-hub-credentials',
                           usernameVariable: 'DOCKER_HUB_USER',
                           passwordVariable: 'DOCKER_HUB_PASSWORD']]) {
          sh """
            docker login -u ${DOCKER_HUB_USER} -p ${DOCKER_HUB_PASSWORD}
            docker build -t johan1a/banking-frontend:dev-${gitCommit} .
            docker build -t johan1a/banking-frontend:dev-latest .
            docker push johan1a/banking-frontend:dev-${gitCommit}
            docker push johan1a/banking-frontend:dev-latest
            """
        }
      }
    }

    stage('Build prd application') {
      container('clojure') {
          sh """
            lein clean
            lein with-profile +uberjar,+prd uberjar
            """
      }
    }

    stage('Build and push prd Docker image') {
      container('docker') {
      withCredentials([[$class: 'UsernamePasswordMultiBinding',
                           credentialsId: 'docker-hub-credentials',
                           usernameVariable: 'DOCKER_HUB_USER',
                           passwordVariable: 'DOCKER_HUB_PASSWORD']]) {
          sh """
            docker login -u ${DOCKER_HUB_USER} -p ${DOCKER_HUB_PASSWORD}
            docker build -t johan1a/banking-frontend:${gitCommit} .
            docker build -t johan1a/banking-frontend:latest .
            docker push johan1a/banking-frontend:${gitCommit}
            docker push johan1a/banking-frontend:latest
            """
        }
      }
    }

    stage('Deploy to dev') {
      container('kubectl') {
        sh "kubectl set image deployment banking-frontend banking-frontend=johan1a/banking-frontend:${gitCommit}"
      }
    }
  }
}
