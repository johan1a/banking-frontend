(ns banking-frontend.handler
  (:require [compojure.core :refer [GET defroutes]]
            [compojure.route :refer [not-found resources]]
            [hiccup.page :refer [include-js include-css html5]]
            [banking-frontend.middleware :refer [wrap-middleware]]
            [config.core :refer [env]]
            [ring.util.response :as r]
            [ring.util.anti-forgery :refer [anti-forgery-field]]
            ))

(def mount-target
  [:div#app
      [:h3 "ClojureScript has not been compiled!"]
      [:p "please run "
       [:b "lein figwheel"]
       " in order to start the compiler"]])

(def bulma "https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.1/css/bulma.css")
(def bulma-min "https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.1/css/bulma.min.css")

(defn config []
    (str (env :env-config)))

(defn head []
  [:head
   [:meta {:charset "utf-8"}]
   [:meta {:name "viewport"
           :content "width=device-width, initial-scale=1"}]
   [:input {:id "app-config" :hidden "hidden" :value (config)}]
   (include-css (if (env :dev) "/css/site.css" "/css/site.min.css"))
   (include-css (if (env :dev) bulma bulma-min))])

(defn loading-page []
  (html5
    (head)
    [:body
     (anti-forgery-field)
     [:section {:class "section"}
      [:div {:class "container"}
         mount-target
         (include-js "/js/app.js")]]]))

(defroutes routes
  (GET "/" [] (loading-page))
  (GET "/about" [] (loading-page))
  (resources "/")
  (not-found "Not Found"))

(def app (wrap-middleware #'routes))
