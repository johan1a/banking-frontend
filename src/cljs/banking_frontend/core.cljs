(ns banking-frontend.core
    (:require-macros [cljs.core.async.macros :refer [go]])
    (:require [reagent.core :as reagent :refer [atom]]
              [secretary.core :as secretary :include-macros true]
              [accountant.core :as accountant]
              [oz.core :as oz]
              [cljs.reader :as edn]
              [cljs-http.client :as http]
              [cljs.core.async :refer [<!]]))

;; -------------------------
(def state (atom {:data [] :auth-token ""}))

(defn config []
  (edn/read-string (.-value (.getElementById js/document "app-config"))))

(def window-width (reagent/atom (.-innerWidth js/window)))

(defn on-window-resize [ evt ]
  (reset! window-width (.-innerWidth js/window)))

(defn overview-url []
  (str (:api-url (config)) "/api/banking/overview"))

(defn api-login-url []
  (str (:api-url (config)) "/api/auth/login"))

(declare go-to-login-page)
(declare go-to-home-page)

(defn get-data []
  (go
      (let [response (<! (http/get (overview-url)
                          {:accept "application/json"
                           :with-credentials? false
                           :headers {"authorization" (str "bearer " (:auth-token @state))}
                           }))]
        (if (= 401 (:status response))
            (go-to-login-page)
            (if (:body response)
                (swap! state assoc :data (:data (:body response))))))))

; Views

(defn line-plot []
  {:data {:values (:data @state)}
   :encoding {:x {:field "dateCreated" :type "temporal"}
              :y {:field "totalOwnCapital" :type "quantitative" :scale {:zero false}}}
   :mark "line"
   :type "fit"
   :resize true
   :width (* @window-width 0.8)
   :height (/ @window-width 5)
   :padding 50
   :autosize { :type "fit" :contains "content" }
   })

(defn home-page []
  (get-data)
  (fn []
    [:div [:h1 {:class "title"} "Welcome to banking-frontend"]
     [:div (str "nbr data points: " (count (:data @state)))]
     [:div {:class "tile"}
      [oz/vega-lite (line-plot)]]
     [:div {:class "subtitle"} [:a {:href "/about"} "go to about page"]]]))


(defn anti-forgery-field []
  [:input {:name "__anti-forgery-token"
           :class "form-control"
           :type "hidden"
           :value (-> js/document
                    (.getElementById "__anti-forgery-token")
                    (.-value))}])

(defn input-element
  [id name type value]
  [:input {:id id
           :name name
           :class "form-control"
           :type type
           :required "true"
           :value @value
           :on-change #(reset! value (-> % .-target .-value))}])


(defn username-input [username-atom]
  (input-element "username" "username" "text" username-atom))

(defn password-input [password-atom]
  (input-element "password" "password" "password" password-atom))

(defn submit-input [username password]
  [:input {:id "loginSubmit"
           :name "loginSubmit"
           :class "form-control"
           :type "submit"
           :required ""
           :value "Submit"
           }])


(defn post-login [username password]
  (go
    (let [body {:username username :password password }
          response (<! (http/post (api-login-url)
                              {:accept "application/json"
                               :with-credentials? false
                               :json-params body}))]
            (if (= 201 (:status response))
                ((swap! state assoc :auth-token (:token (:body response)))
                  (go-to-home-page))
                ))))

(defn login-page []
  (let [username (atom nil)
        password (atom nil)]
  [:div [:h2 {:class "title"} "Log in"]
   [:form { :method "POST"
            :on-submit (fn [e]
                         (.preventDefault e)
                         (post-login @username @password))}
    (anti-forgery-field)
    [:div [username-input username]]
    [:div [password-input password]]
    [:div [submit-input username password]]]
  ]))

(defn about-page []
  [:div [:h2 "About banking-frontend"]
   [:div [:a {:href "/"} "go to the home page"]]])

;; -------------------------
;; Routes

(defonce page (atom #'home-page))

(defn current-page []
  [:div [@page]])

(defn go-to-login-page []
  (reset! page #'login-page))

(defn go-to-home-page []
  (reset! page #'home-page))

(secretary/defroute "/" []
  (go-to-home-page))

(secretary/defroute "/login" []
  (go-to-login-page))

(secretary/defroute "/about" []
  (reset! page #'about-page))

;; -------------------------
;; Initialize app

(defn mount-root []
  (reagent/render [current-page]
                  (.getElementById js/document "app")
                  (.addEventListener js/window "resize" on-window-resize)
                  ))

(defn init! []
  (accountant/configure-navigation!
    {:nav-handler
     (fn [path]
       (secretary/dispatch! path))
     :path-exists?
     (fn [path]
       (secretary/locate-route path))})
    (accountant/dispatch-current!)
    (mount-root))
