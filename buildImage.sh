#!/bin/sh -e

TAG=$(git rev-parse HEAD)

lein clean
lein with-profile +uberjar,+prd uberjar
docker build -t johan1a/banking-frontend:${TAG} .
docker build -t johan1a/banking-frontend:latest .

lein clean
lein compile
lein with-profile +uberjar,+dockerdev uberjar
docker build -t johan1a/banking-frontend:dev-${TAG} .
docker build -t johan1a/banking-frontend:dev-latest .
